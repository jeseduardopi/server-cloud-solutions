<?php

namespace App;

use PDO;

class ConnexionPDO
{
    private $host = 'db';
    // 40.124.182.121
    private $user = 'root';
    private $password = 'example';
    private $db = 'cloud_db';
    private $connexion;

    public function __construct()
    {
        try {
            $this->connexion = new PDO('mysql:host='.$this->host.';dbname='.$this->db, $this->user, $this->password);
        } catch(\Exception $e) {
            die('Erreur : '.$e->getMessage());
        };
    }

    public function getConnexion()
    {
        return $this->connexion;
    }
}
