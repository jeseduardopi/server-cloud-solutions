<?php

namespace App\Entity;

class WebSiteEntity
{
    private int $id;
    private string $name;
    private string $frontStack;
    private string $backendStack;
    private string $serverName;


    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getFrontStack(): string
    {
        return $this->frontStack;
    }

    public function setFrontStack(string $frontStack): void
    {
        $this->frontStack = $frontStack;
    }


    public function getsetBackendStack(): string
    {
        return $this->frontStack;
    }

    public function setBackendStack(string $frontStack): void
    {
        $this->frontStack = $frontStack;
    }



    public function getServerName(): string
    {
        return $this->serverName;
    }

    public function setServerName(string $serverName): void
    {
        $this->serverName = $serverName;
    }
}
