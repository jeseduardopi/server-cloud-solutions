import { NavLink } from "react-router-dom";

export default function Menu() {

    const splitUrl = window.location.href.split('/')
    const url = splitUrl[splitUrl.length - 1]

    const buttonStyles = {
        backgroundColor: "lightblue",
        color: "white",
        padding: "10px 20px",
        margin: "8px",
        marginLeft: "20px", 
        border: "none",
        borderRadius: "5px",
        cursor: "pointer"
    };

    const linkStyles = {
        textDecoration : "none",
        color: "black"
    };

    return (
        <nav>
            <ul>
                <button style={buttonStyles}>
                    {   url === 'login' ? 
                            <NavLink style={linkStyles} to="/new-user">Inscription</NavLink> 
                        :
                            <NavLink style={linkStyles} to="/login">Connexion</NavLink>
                    }
                </button>
            </ul>
        </nav>
    )

}