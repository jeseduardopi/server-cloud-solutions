export default function UserForm({handleSubmit, setUsername, setPassword, children}) {

    const splitUrl = window.location.href.split('/')
    const url = splitUrl[splitUrl.length - 1]

    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    // STYLES

    const buttonStyles = {
        backgroundColor: "lightblue",
        color: "white",
        padding: "10px 20px",
        margin: "8px",
        marginLeft: "20px", 
        border: "none",
        borderRadius: "5px",
        cursor: "pointer"
    };

    const inputStyles = {
        backgroundColor: "white",
        color: "black",
        padding: "5px 10px",
        border: "none",
        fontSize: "16px",
        marginBottom: "20px",
        marginLeft: "15px",
        borderBottom : "2px solid black"
    }

    return (
        <form action="" onSubmit={handleSubmit}>
            <div>
                <label htmlFor="username">Nom d'utilisateur :</label>
                <input style={inputStyles} type="text" id="username" name="username" required onChange={handleUsernameChange} />
            </div>
            <div>
                <label htmlFor="username">Mot de passe :</label>
                <input style={inputStyles} type="password" id="password" name="password" required onChange={handlePasswordChange} />
            </div>
            <button style={buttonStyles} type="submit">{ url === 'login' ? "Se connecter" : "S'inscrire" }</button>
        </form>
    )
}