import {
	BrowserRouter,
	Routes,
	Route
} from "react-router-dom";
import './styles/main.scss';
import NewUser from './routes/new-user'
import DropPage from "./routes/drop-file";
import Login from "./routes/login";
import UserProvider from "./context/UserContext";
import NeedAuth from "./auth/NeedAuth";

function App() {
	return (
		<UserProvider>
			<BrowserRouter>
				<Routes>
					<Route path="/new-user" element={<NewUser />} />
					<Route path='/login' element={<Login/>}/>
					<Route path="/drop-file" element={<DropPage />} />
				</Routes>
			</BrowserRouter>
		</UserProvider>
	)
}

export default App;