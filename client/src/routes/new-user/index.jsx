import useCreateUser from "../../hook/useCreateUser";
import { useState } from "react";
import UserForm from "../../components/UserForm";
import Menu from "../../components/Menu";

export default function NewUser() {
;
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('');

    const createUser = useCreateUser();

    const handleSubmit = (e) => {
        e.preventDefault();
        const userData = {
            username: username,
            password: password 
        }
        createUser(userData).then(data => console.log(data))
    };

    return (
        <div>
            <h1>Bienvenue, inscrivez-vous !</h1>

            <Menu />

            <UserForm handleSubmit={handleSubmit} setPassword={setPassword} setUsername={setUsername} />
        </div>
    )
}