import React, { useState } from "react";
import { useNavigate } from "react-router-dom";


export default function Home() {

    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const navigate = useNavigate();
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    const buttonStyles = {
        backgroundColor: "lightblue",
        color: "white",
        padding: "10px 20px",
        margin: "8px",
        marginLeft: "20px", 
        border: "none",
        borderRadius: "5px",
        cursor: "pointer"
    };

    const linkStyles = {
        textDecoration : "none",
        color: "black"
    };

    const inputStyles = {
        backgroundColor: "white",
        color: "black",
        padding: "5px 10px",
        border: "none",
        fontSize: "16px",
        marginBottom: "20px",
        marginLeft: "15px",
        borderBottom : "2px solid black"
    }


    const handleUsernameChange = (event) => {
        setUsername(event.target.value);
    };

    const handlePasswordChange = (event) => {
        setPassword(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        console.log("Nom d'utilisateur :", username);
        console.log("Mot de passe :", password);

        //Rajouter un if l'utilisateur est bien connecté alors on le redirige vers drop-file
        setIsLoggedIn(true);
        navigate("/drop-file");

    };

    return (
    <div>
        <h1>Bienvenue, connectez-vous !</h1>
        <nav>
            <ul>
                <button style={buttonStyles}><a style={linkStyles} href="/">Connexion</a></button>
                <button style={buttonStyles}><a style={linkStyles} href="/new-user">Inscription</a></button>
            </ul>
        </nav>
        <form onSubmit={handleSubmit}>
        <div> 
            <label htmlFor="username">Nom d'utilisateur :</label>
            <input style={inputStyles} type="text" id="username" value={username} onChange={handleUsernameChange} />
        </div>
        <div>
            <label htmlFor="password">Mot de passe :</label>
            <input style={inputStyles} type="password" id="password" value={password} onChange={handlePasswordChange} />
        </div>
        <button style={buttonStyles} type="submit">Se connecter</button>
        </form>
    </div>
    );
}