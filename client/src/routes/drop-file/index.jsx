import React, { useRef, useState } from "react";
import useAddFile from "../../hook/useAddFile";
import "./index.css";

const DropPage = () => {
    const [fileList, setFileList] = useState([]); 
    const addFile = useAddFile()

    const handleSubmit = (e) => {
        e.preventDefault()
        console.log(fileList)
        addFile(fileList)
    }

    const handleFileChange = (e) => {
        setFileList(e.target.files)
    };

    const buttonStyles = {
        backgroundColor: "lightblue",
        color: "white",
        padding: "10px 20px",
        margin: "8px",
        marginLeft: "20px", 
        border: "none",
        borderRadius: "5px",
        cursor: "pointer"
    };

    return (
        <form className="drop-page" onSubmit={handleSubmit}>
            <input type="file" name="file" id="file" multiple onChange={handleFileChange}/>
            <button style={buttonStyles} type="submit">Ajouter le(s) fichier(s)</button>
        </form>
    );
};

export default DropPage;
