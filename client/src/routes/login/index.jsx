import {useNavigate} from "react-router-dom";
import {useContext, useState} from "react";
import {userContext} from "../../context/UserContext";
import useGetJWT from "../../hook/useGetJWT";
import UserForm from "../../components/UserForm";
import Menu from "../../components/Menu";

export default function Login() {

    const navigate = useNavigate();
    const getJWT = useGetJWT()
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loggedUser, setLoggedUser] = useContext(userContext);

    const handleSubmit = (e) => {
        e.preventDefault();
        getJWT(username, password).then(data => {
            if (data.JWT) {
                setLoggedUser(data.JWT);
                navigate('/drop-file');
            } else {
                console.log(data)
            }
        })
    }

    return (
        <div>
            <h1>Bienvenue, connectez-vous !</h1>

            <Menu />

            <UserForm handleSubmit={handleSubmit} setPassword={setPassword} setUsername={setUsername} />
        </div>
    )
}