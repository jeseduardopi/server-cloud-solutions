export default function useCreateUser() {
    return function (file) {
        return fetch(`http://20.199.45.106/upload-file.php`, {
            method: 'POST',
            body: file,
            mode: 'cors',
            headers: {
              'Access-Control-Allow-Origin':'*'
            }
        })
        .then(response => console.log(response))
    }
}