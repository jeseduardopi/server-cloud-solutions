-- SQL INSTRCUCTIONS
CREATE DATABASE IF NOT EXISTS cloud_db;

USE cloud_db;

# website entity
-- id
-- name
-- frontend
-- backend
-- servername
-- user_id foreign key

CREATE TABLE IF NOT EXISTS user ( id INT NOT NULL AUTO_INCREMENT, email VARCHAR(150) NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id) );


CREATE TABLE IF NOT EXISTS website ( id INT NOT NULL AUTO_INCREMENT, name VARCHAR(150) NOT NULL, frontend VARCHAR(255) NOT NULL, backend VARCHAR(255) NOT NULL, servername VARCHAR(255), user_id INT NOT NULL, PRIMARY KEY(id), INDEX(user_id), CONSTRAINT FK_UserWebsite FOREIGN KEY(user_id) REFERENCES user(id));




# user
-- id
-- username
-- email
-- password


#### INSERTIONS TEST

INSERT INTO user (email, username, password)
VALUES('douglas@mail.test', 'douglastest', 'mdptest'),
('marie@mail.test', 'marietest', 'mdptest'),
('jeremy@mail.test', 'jeremytest', 'mdptest')
;



